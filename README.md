# Work Assurance

This application helps to detect a man on image by reference photo.
The main purpose of this project is to detect a man on a working place.
In first case (when worker is on place), application will highlight his face with green rectangle.
In second case (when worker is gone and not doing his job), application will give you a mobile number of a bad worker and will cross the image with a red lines.

In this demo we will be checking the Dude.
Determining that the work of Dude is chilling with his friends, in needs of demonstration, we will load images from bouling club and searching the Dude, and the images from office.
Let's take a look, how this application works.

Login screen.
![Login](https://bytebucket.org/NikChe71/work_assurance/raw/53560abdef85cd4283280b4e4c69c45cf45d33b2/login.PNG)
Main window.
![Main](https://bytebucket.org/NikChe71/work_assurance/raw/53560abdef85cd4283280b4e4c69c45cf45d33b2/main_window.PNG)
Good result.
![Good](https://bytebucket.org/NikChe71/work_assurance/raw/53560abdef85cd4283280b4e4c69c45cf45d33b2/detection_good_result.PNG)
Bad result.
![Bad0](https://bytebucket.org/NikChe71/work_assurance/raw/53560abdef85cd4283280b4e4c69c45cf45d33b2/detection_bad_result_0.PNG)
![Bad1](https://bytebucket.org/NikChe71/work_assurance/raw/53560abdef85cd4283280b4e4c69c45cf45d33b2/detection_bad_result_1.PNG)