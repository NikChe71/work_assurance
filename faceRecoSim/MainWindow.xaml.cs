﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Globalization;
// References for parsing WEB
using System.Net;
using System.Net.Http;
using System.IO;
using RestSharp;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
// References for webcam and data type Dictionary
using Microsoft.Expression.Encoder.Devices;
using System.Collections.ObjectModel;
// Reference for image capturing
using Emgu.CV;
using Emgu.CV.Structure;

namespace faceRecoSim
{
    public class Requester
    {
        public Requester() { }

        // Method for posting images to service FaceFeatureExtraction
        // Returns filtered string that contains feature vectors
        public string cURL_request(string batch_file)
        {

            // POST to face detection service. cURL
            Process p = new Process();

            // Setting up all options
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;

            // Passing .bat file to run it in cli
            p.StartInfo.FileName = batch_file;

            // Hide console from user
            p.StartInfo.CreateNoWindow = true;

            // Enable any event that will be raised by this .bat
            p.EnableRaisingEvents = true;

            // Running .bat file
            p.Start();

            // String variable for storind STDout
            string cmdCURL_responce = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            // Console.WriteLine(cmdCURL_responce);

            string response = cmdCURL_responce.ToString();
            // Using regular expression to cut all noisy info
            Regex regex = new Regex(@"C:\\.*.jpg\""", RegexOptions.Compiled);
            string regex_result = regex.Replace(response, "");
            // Console.WriteLine(regex_result);

            // Responce that is filtered by regular expression
            return regex_result;

            // Main responce out from cmd.
            // return cmdCURL_responce;
        }

        // Alternative method for posting random images using cURL request
        public string curlRequest(string image_uri = @"C:\Users\i331722\Documents\CODE\C#\WorkAssurance\utils\photo\current_image.jpg")
        {
            // Creating a new process instance
            Process p = new Process();
            // Passing .exe file to start a command line
            p.StartInfo.FileName = @"C:\windows\system32\cmd.exe";
            //p.StartInfo.Arguments = @"/c curl -s --request POST  --url ""https://sandbox.api.sap.com/ml/facefeatureextraction/face-feature-extraction""  --header ""APIKey: EltkXHjLOtBCTmurAqSGZ382WwkTOwhP""  --header ""Accept: application/json""  --header ""content-type: multipart/form-data; boundary=---011000010111000001101001"" --form ""files=@C:\Users\i331722\Documents\CODE\C#\WorkAssurance\utils\photo\current_image.jpg""";
            // Passing arguments to command line
            p.StartInfo.Arguments = string.Format(@"/c curl -s --request POST  --url ""https://sandbox.api.sap.com/ml/facefeatureextraction/face-feature-extraction""  --header ""APIKey: EltkXHjLOtBCTmurAqSGZ382WwkTOwhP""  --header ""Accept: application/json""  --header ""content-type: multipart/form-data; boundary=---011000010111000001101001"" --form ""files=@{0}""", image_uri);
            // Setting up options for getting info out of cmd
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.EnableRaisingEvents = true;
            // Starting a reqest
            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            Console.WriteLine(output);
            
            return output;
        }

        public (string, Dictionary<string, string>) parsingImageVectors(string JsonString, string ref_OR_curr = "curr") // Choose which id will be used by loading vectors to similarity service
        {
            // Automaticaly parse json into object
            JObject imageCurrent_jsonobj = JObject.Parse(JsonString);

            // Add a variable for labeling vectors
            string label = ref_OR_curr;

            // Creating buffer for storing face vectors
            Dictionary<string, string> faceVectors = new Dictionary<string, string>();
            // Buffer for storing location of faces
            Dictionary<string, string> faceLocations = new Dictionary<string, string>();

            // Counter for adding special symbols in correct places and changing vectors names
            int count = 0;
            // Iterate through all elements in JSON called "faces" to get their feature vectors and locations
            foreach (object element in imageCurrent_jsonobj["predictions"][0]["faces"])
            {   
                /*
                // Here is THE EVILEST way to store DATA
                // I save LABEL of vector in VALUE field
                // And VECTOR in key field
                // Need to fix it as soon as possible!!!
                
                string vector = JObject.Parse(element.ToString())["face_feature"].ToString();
                faceVectors.Add(vector, label + count);
                string location = JObject.Parse(element.ToString())["face_location"].ToString();
                faceLocations.Add(location, label + count);
                
                // Getting key by a value
                string value = String.Format("{0}{1}", label, count);
                var myKey = faceVectors.FirstOrDefault(x => x.Value == value).Key;
                Console.WriteLine(myKey);
                Console.WriteLine(faceVectors.Values);
                */

                string vector = JObject.Parse(element.ToString())["face_feature"].ToString();
                faceVectors.Add(label + count, vector);
                string location = JObject.Parse(element.ToString())["face_location"].ToString();
                faceLocations.Add(label + count, location);

                count += 1;
            }

            // Creating request body for Similarity scoring service
            string insertVector = "";
            // Counter for adding special symbols in correct places and changing vectors names
            count = 0;
            // Iterate through all parsed vectors
            foreach (string vector in faceVectors.Keys)
            {
                // Adding an exception for last vector to not paste comma after its body
                if (count != faceVectors.Count - 1)
                {
                    insertVector += String.Format("\n{{\"id\": \"{0}\", \"vector\": {1}}}", vector, faceVectors[vector]) + ",";
                    // Console.WriteLine(insertVector);
                    count += 1;
                }
                else
                {
                    insertVector += String.Format("\n{{\"id\": \"{0}\", \"vector\": {1}}}", vector, faceVectors[vector]);
                }
            }
            // Console.WriteLine(insertVector);
            return (insertVector, faceLocations);
        }

        // Method for adding all vectors (inherited in string) into request body.
        public string compileRequest(string reference_vector, string current_vector)
        {
            // Inserting vectors in json request body
            string request_body = String.Format("------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"texts\"\r\n\r\n{{ \"0\": [{0}], \"1\": [{1}] }}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"options\"\r\n\r\n{{\"numSimilarVectors\":2}}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"files\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", reference_vector, current_vector);
            // Console.WriteLine(request_body);
            return request_body;
        }

        // Method for posting vectors to similarity scoring service
        public string post_JSON(string JSON_data)
        {
            // Checking which vectors are similar by posting data to api
            var client_similarity = new RestClient("https://sandbox.api.sap.com/ml/similarityscoring/similarity-scoring");
            var request_similarity = new RestRequest(Method.POST);
            request_similarity.AddHeader("Cache-Control", "no-cache");
            request_similarity.AddHeader("APIKey", "EltkXHjLOtBCTmurAqSGZ382WwkTOwhP");
            request_similarity.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
            request_similarity.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", JSON_data, ParameterType.RequestBody);
            IRestResponse response_similarity = client_similarity.Execute(request_similarity);


            // Console.WriteLine(response_similarity.Content);

            return response_similarity.Content;
        }

        public string resultFilter(string responce)
        {
            Regex filterID = new Regex(@"\""id\"": \"".*\""", RegexOptions.Compiled);
            MatchCollection result = filterID.Matches(responce);
            // var segment = new ArraySegment<MatchCollection>(result, 1,3);
            foreach (var element in result)
            {
                Console.WriteLine(element.ToString());
                Console.WriteLine();
            }
            return "result";
        }

        public (int, int, int, int, bool) parseCoordinates(string serviceResponce, Dictionary<string, string> locations)
        {
            // Filter responce and finding out what is the best possible match
            Regex filterBM = new Regex(@"curr.", RegexOptions.Compiled);
            string bestMatch = filterBM.Match(serviceResponce).ToString();
            string stringCoordinates = locations[bestMatch];

            // Check whether is reference face on image or not
            Regex filterDraw = new Regex(@"0\.(\d+)", RegexOptions.Compiled);
            string drawORnot_value = filterDraw.Match(serviceResponce).ToString();
            float value = float.Parse(drawORnot_value.ToString(), CultureInfo.InvariantCulture.NumberFormat);

            bool workerIsOnPlace = false;

            if (value >= 0.9)
            {
                Regex filterCoords = new Regex(@"\d+", RegexOptions.Compiled);
                MatchCollection coordinates = filterCoords.Matches(stringCoordinates);

                int bottom = Int32.Parse(coordinates[0].ToString());
                int left = Int32.Parse(coordinates[1].ToString());
                int right = Int32.Parse(coordinates[2].ToString());
                int top = Int32.Parse(coordinates[3].ToString());
                workerIsOnPlace = true;
                return (bottom, left, right, top, workerIsOnPlace);
            }
            else
            {
                int bottom = 0;
                int left = 0;
                int right = 0;
                int top = 0;
                MessageBox.Show("The employee is not working.\nHis phone number: +7(890)123-45-67");
                return (bottom, left, right, top, workerIsOnPlace);
            }
        }

        public System.Windows.Media.ImageSource drawOnImage(string link, int left, int bottom, int right, int top)
        {
            // Draw on image
            System.IO.FileInfo fi = new System.IO.FileInfo(link);
            BitmapImage src = new BitmapImage();
            src.BeginInit();
            src.UriSource = new Uri(fi.FullName, UriKind.Absolute);
            src.CacheOption = BitmapCacheOption.OnLoad;
            src.EndInit();

            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                dc.DrawImage(src, new Rect(0, 0, src.PixelWidth, src.PixelHeight));

                //SolidColorBrush rectBrush = new SolidColorBrush(Colors.GreenYellow);
                SolidColorBrush rectBrush = new SolidColorBrush(Colors.LawnGreen);
                rectBrush.Opacity = 0.5;
                dc.DrawRectangle(rectBrush, null, new Rect(left, top, right, bottom));
            }

            RenderTargetBitmap rtb = new RenderTargetBitmap(src.PixelWidth, src.PixelHeight, 96, 96, PixelFormats.Pbgra32);
            rtb.Render(dv);
            return rtb;
        }
    }

        /// <summary>
        /// Interaction logic for MainWindow.xaml
        /// </summary>
    public partial class MainWindow : Window
        {
        
        public MainWindow()
            {
            InitializeComponent();
            this.Title = "PochtaBank Demo";
        }
        
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            // Hide previous fail
            cross0.Visibility = Visibility.Hidden;
            cross1.Visibility = Visibility.Hidden;

            // Creating new instance of class OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Adding title to dialog frame
            dlg.Title = "Select image";
            // Filtering all files that are placed in search folder
            dlg.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
            "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
            "Portable Network Graphic (*.png)|*.png";
            // Creating new variable for storing choosen image
            BitmapImage image = new BitmapImage();
                
            // Menu for choosing image
            dlg.ShowDialog();
            string link = dlg.FileName;

            // Hide labels 'Image'
            label_image0.Visibility = Visibility.Hidden;
            label_image1.Visibility = Visibility.Hidden;

            // Passing choosen image as current image
            image_faces.Source = new BitmapImage(new Uri(link));
            // Center current image inside the box
            //image_faces.Stretch = Stretch.None;
            //image_faces.HorizontalAlignment = HorizontalAlignment.Center;
            //image_faces.VerticalAlignment = VerticalAlignment.Center;

            // Hardcoded value of reference image for secure needs.
            // No one can modify this image. Only administrator.
            // This image will be loaded later from DB.
            referenceImageIcon.Source = new BitmapImage(new Uri(@"C:\Users\i331722\Documents\CODE\C#\faceRecoSim\faceRecoSim\backend\utils\photo\reference_photo\reference_image.jpg"));
            // Center reference image in icon
            //referenceImageIcon.Stretch = Stretch.Uniform;
            //referenceImageIcon.HorizontalAlignment = HorizontalAlignment.Center;
            //referenceImageIcon.VerticalAlignment = VerticalAlignment.Center;

            // Creating new instance of class Requester that contains necessary methods 
            Requester req = new Requester();

            // Update progress bar
            this.analyzeProgressBar.Value = 10;
            UpdateLayout();

            // Links to curl requests for random image types. (Troubles with passing arguments into cmd |._.| )
            string reference_image = @"C:\Users\i331722\Documents\CODE\C#\faceRecoSim\faceRecoSim\backend\utils\featurePhoto_ref.bat";
            string current_image = @"C:\Users\i331722\Documents\CODE\C#\faceRecoSim\faceRecoSim\backend\utils\featurePhoto_curr.bat";

            // Vectorizing and parsing reference image
            string image_features_ref = req.cURL_request(reference_image);
            (string data_ref, Dictionary<string, string> _) = req.parsingImageVectors(image_features_ref, "ref");

            // Update progress bar
            this.analyzeProgressBar.Value = 40;
            UpdateLayout();

            // Testing new method
            string image_features_curr = req.curlRequest(link);
            // Vectorizing and parsing current image
            //string image_features_curr = req.cURL_request(current_image);
            (string data_curr, Dictionary<string, string> faceLocations) = req.parsingImageVectors(image_features_curr, "curr");

            // Update progress bar
            this.analyzeProgressBar.Value = 80;
            UpdateLayout();

            // Compiling and posting request
            string data = req.compileRequest(data_ref, data_curr);
            // Request to similarity service
            string similarity_results = req.post_JSON(data);
            
            if (detailsBox.IsChecked == true)
            {
                // Show result of matching in message box
                MessageBox.Show(similarity_results);
            }
            else
            {
                Console.Write(similarity_results);
            }
            

            // Coordinates of faces with highest similarity score
            // Filtering matches and choosing best one
            // The best goes first so we choose simple Match method
            // for cropping fisrst occurence
            (int bottom, int left, int right, int top, bool workerIsOnPlace) = req.parseCoordinates(similarity_results, faceLocations);
            
            if (workerIsOnPlace == true)
            {
                cross0.Visibility = Visibility.Hidden;
                cross1.Visibility = Visibility.Hidden;
                // Draw rectangle on image
                //image_faces.Source = req.drawOnImage(link, left, bottom, right, top);
                image_faces.Source = req.drawOnImage(link, left, top, right - left, bottom - top);
            }
            else
            {
                cross0.Visibility = Visibility.Visible;
                cross1.Visibility = Visibility.Visible;
            }
            
            
            // Update progress bar
            this.analyzeProgressBar.Value = 100;
            UpdateLayout();
        }

        private void CMD_Click(object sender, RoutedEventArgs e)
        {
            Requester req = new Requester();
            string output = req.curlRequest();
            MessageBox.Show(output);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Close window
            this.Close();
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource iImageViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("iImageViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // iImageViewSource.Source = [generic data source]
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            /*
            // Frame
            Image<Bgr, byte> current;
            // Webcam
            try
            {
                Emgu.CV.Capture webcam = new Emgu.CV.Capture(camIndex: 0);

                current = webcam.QueryFrame();
                // Flip image, because forecam is MIRRORED
                current = current.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);
                Image<Bgr, byte> webCamImage = new Image<Bgr, byte>(current.Bitmap);
                webCamImage.Save("snapshot.jpg");
            }
            catch (Exception exc)
            {
                MessageBox.Show("This option is unavailable at this moment.\n", exc.ToString());
            }
            Console.Write("Error occured.");
            */

            Process runWebCamForms = new Process();
            runWebCamForms.StartInfo.FileName = @"C:\Users\i331722\source\repos\MailBank\MailBank\bin\Debug\MailBank.exe";
            runWebCamForms.Start();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            loginRectangle.Visibility = Visibility.Hidden;
            loginButton.Visibility = Visibility.Hidden;
            loginLabel.Visibility = Visibility.Hidden;
            loginLabelName.Visibility = Visibility.Hidden;
            loginLabelPassword.Visibility = Visibility.Hidden;
            loginName.Visibility = Visibility.Hidden;
            loginPassword.Visibility = Visibility.Hidden;
            loginRectangle.Visibility = Visibility.Hidden;
        }
    }
}
