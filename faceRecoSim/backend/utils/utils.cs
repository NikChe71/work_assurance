using System;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Text;
using System.Collections.Generic;
using RestSharp;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
// using Emgu.CV;

namespace faceRecoSim
{
    public class Requester
    {
        public Requester() { }

        // Method for posting images to service FaceFeatureExtraction
        // Returns filtered string that contains feature vectors
        public string cURL_request(string batch_file)
        {

            // POST to face detection service. cURL
            Process p = new Process();

            // Setting up all options
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;

            // Passing .bat file to run it in cli
            p.StartInfo.FileName = batch_file;

            // Enable any event that will be raised by this .bat
            p.EnableRaisingEvents = true;

            // Running .bat file
            p.Start();

            // String variable for storind STDout
            string cmdCURL_responce = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            // Console.WriteLine(cmdCURL_responce);

            string response = cmdCURL_responce.ToString();
            // Using regular expression to cut all noisy info
            Regex regex = new Regex(@"C:\\.*.jpg\""", RegexOptions.Compiled);
            string regex_result = regex.Replace(response, "");
            // Console.WriteLine(regex_result);

            // Responce that is filtered by regular expression
            return regex_result;

            // Main responce out from cmd.
            // return cmdCURL_responce;
        }

        public string parsingImageVectors(string JsonString, string ref_OR_curr = "curr") // Choose which id will be used by loading vectors to similarity service
        {
            // Automaticaly parse json into object
            JObject imageCurrent_jsonobj = JObject.Parse(JsonString);

            // Add a variable for labeling vectors
            string label = ref_OR_curr;

            // Creating buffer for storing face vectors
            Dictionary<string, string> faceVectors = new Dictionary<string, string>();

            // Counter for adding special symbols in correct places and changing vectors names
            int count = 0;
            // Iterate through all elements in JSON called "faces" to get their feature vectors
            foreach (object element in imageCurrent_jsonobj["predictions"][0]["faces"])
            {
                string vector = JObject.Parse(element.ToString())["face_feature"].ToString();
                faceVectors.Add(vector, label + count);
                count += 1;
            }

            // Creating request body for Similarity scoring service
            string insertVector = "";
            // Counter for adding special symbols in correct places and changing vectors names
            count = 0;
            // Iterate through all parsed vectors
            foreach (string vector in faceVectors.Keys)
            {
                // Adding an exception for last vector to not paste comma after its body
                if (count != faceVectors.Count - 1)
                {
                    insertVector += String.Format("\n{{\"id\": \"{0}\", \"vector\": {1}}}", faceVectors[vector], vector) + ",";
                    // Console.WriteLine(insertVector);
                    count += 1;
                }
                else
                {
                    insertVector += String.Format("\n{{\"id\": \"{0}\", \"vector\": {1}}}", faceVectors[vector], vector);
                }
            }
            // Console.WriteLine(insertVector);
            return insertVector;
        }

        // Method for adding all vectors (inherited in string) into request body.
        public string compileRequest(string reference_vector, string current_vector)
        {
            // Inserting vectors in json request body
            string request_body = String.Format("------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"texts\"\r\n\r\n{{ \"0\": [{0}], \"1\": [{1}] }}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"options\"\r\n\r\n{{\"numSimilarVectors\":2}}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"files\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", reference_vector, current_vector);
            // Console.WriteLine(request_body);
            return request_body;
        }

        // Method for posting vectors to similarity scoring service
        public string post_JSON(string JSON_data)
        {
            // Checking which vectors are similar by posting data to api
            var client_similarity = new RestClient("https://sandbox.api.sap.com/ml/similarityscoring/similarity-scoring");
            var request_similarity = new RestRequest(Method.POST);
            request_similarity.AddHeader("Cache-Control", "no-cache");
            request_similarity.AddHeader("APIKey", "EltkXHjLOtBCTmurAqSGZ382WwkTOwhP");
            request_similarity.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
            request_similarity.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", JSON_data, ParameterType.RequestBody);
            IRestResponse response_similarity = client_similarity.Execute(request_similarity);

            // Console.WriteLine(response_similarity.Content);

            return response_similarity.Content;
        }

        public string resultFilter(string responce)
        {
            Regex filterID = new Regex(@"\""id\"": \"".*\""", RegexOptions.Compiled);
            MatchCollection result = filterID.Matches(responce);
            // var segment = new ArraySegment<MatchCollection>(result, 1,3);
            foreach (var element in result)
            {
                Console.WriteLine(element.ToString());
                Console.WriteLine();
            }
            return "result";
        }

        public void showImage()
        {

        }
    }
}