﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using System.Linq;


namespace faceRecoSim
{
    class Program
    {
        static void Main(string[] args)
        {

            // Creating new instance of class Requester that contains necessary methods 
            Requester req = new Requester();
            
            // Links to curl requests for random image types. (Troubles with passing arguments into cmd |._.| )
            string reference_image = @"C:\Users\i331722\Documents\CODE\C#\WorkAssurance\utils\featurePhoto_ref.bat";
            string current_image = @"C:\Users\i331722\Documents\CODE\C#\WorkAssurance\utils\featurePhoto_curr.bat";
            
            // Vectorizing and parsing reference image
            string image_features_ref = req.cURL_request(reference_image);
            string data_ref = req.parsingImageVectors(image_features_ref, "ref");

            // Vectorizing and parsing current image
            string image_features_curr = req.cURL_request(current_image);
            string data_curr = req.parsingImageVectors(image_features_curr, "curr");
            // string data_curr = req.parseVectors(image_features_curr);

            // Compiling and posting request
            string data = req.compileRequest(data_ref, data_curr);
            // Request to similarity service
            string similarity_results = req.post_JSON(data);
            Console.WriteLine(similarity_results);

            // Filtering data and returning main result
            // string result = req.resultFilter(similarity_results);
            // Console.WriteLine(result);
        }
    }
}
